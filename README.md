![markdown image](https://blogthinkbig.com/wp-content/uploads/sites/4/2020/09/Markdown-Logo-Example-Markdown-Preview-Enhanced.jpg?fit=1500%2C1000)

# MarkdownUtil

## Description

Host your ".md" files and use the url to those files to generate an HTML page.

## Usage

mod markdown_util;

```rust
#[tokio::main]
async fn main() {
    let url = "https://gitlab.com/dshamany/rustscaffold/-/raw/main/README.md";
    let html_content = markdown_util::to_html_string(url).await;

    match html_content {
        Ok(data) => {
            println!("{}", data);
        }
        Err(e) => {
            println!("{}", e);
        }
    }
}
```

PS! See the [main.rs](https://gitlab.com/dshamany/markdownutil/-/blob/main/src/main.rs?ref_type=heads) file for a more advanced approach to avoid making two separate HTTP reqwests.

## Add MarkdownUtil to your project

Copy `/src/markdown_util` to your `/src` directory and import it using `mod markdown_util;`.

## Dependencies

- markdown 
- regex
- reqwest
- serde_json

## Functions

#### Async

Async is preferred to be used with Actix-Web, Rocket, Warp and more. It allows the function to be non-blocking and conforms to the `reqwest` Client.

#### `get_markdown_document(filename: &str) -> Result<String, String>`

Download a markdown file from any valid url. The `is_markdown_file` will check the extension to make sure it's a ".md" file.

#### `to_value(from_markdown_url: &str) -> Result<Value, String>`

`to_value` is used for server side rendering. It extracts the title along with any tags in the format "#tag". It also extracts the first image in the document, which is helpful if you wish to render the image separately.

###### Output
    {
        "title": String,
        "image": {
            "url": String,
            "alt": String,
        },
        "tags": [String],
        "content": String
    } 

You can use this function if you're rendering your markdown to a custom template.

#### `content_to_html_string(from_markdown_url) -> Result<String, String>`

`content_to_html_string` uses `to_value` to return the content in html.

#### value_to_html_string(content_from_value) -> Result<String, String>

This function is used so that we don't have to make two separate requests if we want a cleaned up content and the metadata associated with the file.

The `Value` type passed into it must be a String or it will return an error.

#### Error Handling

All errors return a string to describe the error for simplicity.

#### Content Cleanup

The `cleanup_content` function is used to remove any whitespaces and quotations that the `Value` type tends to produce. Ideally, we wish to return a `Value` and use the content from it to render our HTML.

---

#### IMPORTANT!

The `markdown` crate used here does not currently support inline HTML which is partly the reason why this code is as suprisingly complicated as the girl you dated in college. Seems simple on the outside, but contains caveats.
