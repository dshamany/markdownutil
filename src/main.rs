mod markdown_util;

#[tokio::main]
async fn main() {
    let url = "https://gist.githubusercontent.com/rt2zz/e0a1d6ab2682d2c47746950b84c0b6ee/raw/83b8b4814c3417111b9b9bef86a552608506603e/markdown-sample.md";

    let raw_html = markdown_util::to_raw_html(url).await;

    match raw_html {
        Ok(html) => {
            // Remove inline HTML as it is not supported
            println!("{}", html);
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
}
